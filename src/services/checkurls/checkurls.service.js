// Initializes the `checkurls` service on path `/checkurls`
const { Checkurls } = require('./checkurls.class');
const hooks = require('./checkurls.hooks');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/checkurls', new Checkurls(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('checkurls');

  service.hooks(hooks);
};
