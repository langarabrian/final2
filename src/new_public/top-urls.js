(function () {
  'use strict';

  // deal with Google Cloud Functions having a different service prefix
  const getServicePrefix = () => {
      const lastSlashPos = window.location.pathname.lastIndexOf('/');
      if ( lastSlashPos < 1 ) return '';
      else return window.location.pathname.substring( 1, lastSlashPos+1 );
  }

  // add row to table of URLs -- you can use this later
  const addURL = url => {
    $('#urls > tbody:last-child').append(
      `<tr>
        <td>${url.count}</td>
        <td>${url.url}</td>
      </tr>`
    );
  }

  window.addEventListener('load', async function () {
    // Set up FeathersJS app
    var app = feathers();

    // Set up REST client
    var restClient = feathers.rest();

    // Configure an AJAX library with that client
    app.configure(restClient.fetch(window.fetch));

    // get handle to service
    const urls = app.service( getServicePrefix() + 'urls');

    /////////////
    // START HERE
    // EXAM Q. 14
    /////////////

  }, false);
}());
